function counterFactory() {
  let currentValue = 0;
  let increment = function () {
    currentValue = currentValue + 1;
    return currentValue;
  };

  let decrement = function () {
    currentValue = currentValue - 1;
    return currentValue;
  };

  return { increment: increment, decrement: decrement };
}

module.exports = counterFactory;
