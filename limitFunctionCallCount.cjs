function limitFunctionCallCount(callBack, limit) {
  let count = 0;

  if (typeof callBack === "function" && typeof limit === "number") {
    return function invokesCallBack(...args) {
      if (count < limit) {
        count = count + 1;
        return callBack(...args);
      } else {
        return null;
      }
    };
  } else {
    throw new Error("wrong parameters passed");
  }
}

module.exports = limitFunctionCallCount;
