const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");
function callBack() {
  return "callback function called ";
}
const test = limitFunctionCallCount(callBack, 2);

try {
  console.log(test());
  console.log(test());
  console.log(test());
  console.log(test());
} catch (error) {
  console.error(error);
}
