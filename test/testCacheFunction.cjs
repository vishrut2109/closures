const cacheFunction = require("../cacheFunction.cjs");

function callBack() {
  console.log("callback funnction called");
}
const name = "Bruce Wayne";
const age = 36;
const location = "Gotham";

const test = cacheFunction(callBack);

console.log(test());
console.log(test(8));
console.log(test(2));
console.log(test(5));
