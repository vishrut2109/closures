function cacheFunction(callBack) {
  let cache = {};

  if (typeof callBack === "function") {
    return function invokesCallBack(...args) {
      if (cache[args]) {
        console.log("old entry: callback not invoked");
        return cache[args];
      } else {
        console.log("new entry: invoking callback");
        cache[args] = callBack(...args);
        return cache[args];
      }
    };
  } else {
    throw new Error("wrong parameters passed");
  }
}

module.exports = cacheFunction;
